/*
    Enum of all available actions.
    Business logic operations
*/

export const RESET_TRANSACTION = "resetTransaction"
export const SAVE_TRANSACTION = "saveTransaction"
